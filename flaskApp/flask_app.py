from flask import Flask
from flask_restplus import Resource, Api
import joblib
from flask.templating import render_template

app = Flask(__name__)
api = Api(app)

model = joblib.load('exp_salary.joblib')

@api.route('/<country>/<float:mpi>/<currency>')
class getLoan(Resource):
    def get(self, country, mpi,currency):
        print('Query for : Country: '+country+' MPI: '+ str(mpi)+' currency: '+currency)
        amount = model.predict([[country,mpi, currency]])
        return {'Able to get loan':amount[0]}


if __name__ == '__main__':
    print('hello')
    app.run(debug=True)